import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {

  @Input() public width: string = '100px';
  @Input() public height: string = '30px';
  @Input() public text: string = '';
  @Input() public backgroundColor: string = '#3dbadf';
  @Output() public onButtonClickEvent: EventEmitter<void> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {}

  public onButtonClick(): void {
    this.onButtonClickEvent.emit();
  }

}
