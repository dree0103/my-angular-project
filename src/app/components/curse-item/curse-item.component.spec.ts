import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CurseItemComponent } from './curse-item.component';

describe('CurseItemComponent', () => {
  let component: CurseItemComponent;
  let fixture: ComponentFixture<CurseItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CurseItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CurseItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
