import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-curse-item',
  templateUrl: './curse-item.component.html',
  styleUrls: ['./curse-item.component.css']
})
export class CurseItemComponent implements OnInit {

  @Input() public text: string = '';
  @Input() public title: string = '';
  @Input() public loadMore: boolean = false;

  constructor() { }

  ngOnInit(): void {}

  public onButtonClick(buttonName: string): void {
    console.log(`${buttonName} button was clicked`)
  }

}
