import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchComponent implements OnInit {

  public inputValue: string = "";

  constructor() { }

  ngOnInit(): void {
  }

  public onInputValueChange(event: any): void {
    this.inputValue = event.target.value;
  }

  public onSearchClick(): void {
    console.log(this.inputValue);
  }

}
