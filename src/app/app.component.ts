import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public curseItem: {[key: string]: string} = {
    title: 'Video Course 1. Name tag',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.' +
          ' Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor' +
          ' in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
  }
  public curses = [{...this.curseItem}, {...this.curseItem}, {...this.curseItem}]

  constructor() {}

  public trackByIndex(index: number): number {
    return index;
  }

  public onButtonClick(buttonName: string): void {
    console.log(`${buttonName} button was clicked`)
  }
}
